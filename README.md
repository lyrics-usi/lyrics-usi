# LYRICS

## Introduction
The Lyrics framework allows to inject logic knowledge into a learner using the Semantic Based Regularization framework:
1. M Diligenti, M Gori, C Sacca, "Semantic-based regularization for learning and inference", Artificial Intelligence 244, 143-165, 
2. G Marra, F Giannini, M Diligenti, M Gori, "LYRICS: A General Interface Layer to Integrate Logic Inference and Deep Learning", Joint European Conference on Machine Learning and Knowledge Discovery in Databases, 283-298, 2019.

LYRICS is a TensorFlow library, which defines a declarative language to express the prior knowledge about a learning task, which can be injected into any tensorflow learner. The declarative language allows to define any many–sorted logical theory, namely you can declare some domains of different sort, with constants, functions and relations on them.

### Tutorial Content.
Details about the provided examples in the tutorial
1. "manifold.ipynb" is an example on how to implement manifold regularization by means of logical constraints;
2. "deduction.ipynb" is a naive example on how infer logical rules from data by model checking;
3. "collective.ipynb" is an example on how to implement collective classification using our logical framework.

### Exucution via Docker
How to use the Docker container to run a tutorial:
1. Install [Docker] (https://docs.docker.com/get-docker/) and [Docker Compose] (https://docs.docker.com/compose/install/) (the two tools ship together on Windows and OSX)
2. Open a terminal in the tutorial main folder (the one containing the "Dockerfile")
3. Executing the command:
    * docker-compose up
This will:

* Download a base image for the virtualize environment (just the first time)
* Configure the container as specified in the "Dockerfile"
* Run the Jupyter notebook server inside the container

If everything goes well, you should see on the terminal the execution of the Jupyter notebook

Copying the last link (the one starting with "http://127.0.0.1:8888") on the address bar of a browser will allow you to access the tutorial.

Once you are done, pressing CTRL+C on the terminal will close the Docker container.

### Exucution on Binder
The tutorial can also be executed online on binder at the link:
https://mybinder.org/v2/gl/lyrics-usi%2Flyrics-usi/HEAD
